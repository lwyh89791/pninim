% שאלה ךב

ברודא עש"ק כ"ב אדר תפד לפ"ק

*צפרא* טבא לגברא רבא ויקירא דחיי וחמריה מיזן זיין זיינו שולף לקבל אלף כו' כמהורר יעקב נר"ו:

*מכאן* מודעא רבא לאורייתא ויקרא דיליה וכו' שנשאל שם קושיא עצומה ונשאר אצל כל הלומדים בצ"ע. וזהו בי"ד סימן קמ"ה כתב הש"ך בסעיף קטן ב' והוכיח מש"ס ע"ז (דמ"ז) דמים תלושין של רבים אסירי עכ"ל. והנה בריש (דנ"ט) קאמר הש"ס לא צריכה דטפחינהו כו' מוכח דדוקא מים תלושין של יחיד אסירי. משא"כ מים תלושין של רבים מותרים ע"כ. ובקשתי מה שיעלה במצודת שכלו הרחב ישוב על קושיא זו יודיעני ואעשה עטרה לראשי וענק לגרגרותי. ושלום מאד"ה וממני אהובו לנצח המר והנאנח על המטה מונח. וכותב בידים רפות מרדכי לייפניק:

*תשובה*

[^1]*מתוך* עומק הטרדות אומר הנלע"ד לפום ריהטא לתשובת הקושיא הנזכרת שנראית חזקה לכאורה. איברא כי דייקינן משכחינן דשמעתיה דמר הרב בש"ך ז"ל נהירא טובא: ואדרבא ממקום שבאו לסתור דבריו משם יש לו סיוע שיש בו ממש לענ"ד הקלושה כאשר אבאר. ותחלה אומר שלדעתי דינו של הרב ז"ל פשוט ומאומת מהא דתנן סתמא וכל שיש בו תפיסת ידי אדם אסור. ש"מ בהדיא דתלושין אסירי ולא שנא של רבים או של יחיד. כי היכי דלא אשכחן באבני הר ודכוותייהו שום חילוק בזה. ותדע דכללא הוא ולמדין מכלל זה לעניננו. דהא תלמודא מדמי להו להדדי כדס"ד דר"י מיירי בדתלשינהו גל. דאמרינן סוף סוף אבני הר שנדלדלו נינהו. וא"כ בתלושין גמורין לית דין ולית דיין דהיינו אבני הר שנעקרו בידי אדם דבכל גוונא אסירי. וקצת תימא על הרב ש"ך שלא נסתייע מזה. עכ"פ פשוט וברור דתלושין. תפיסת ידי אדם יש בהם. וא"כ איך לא יהסרו {לוח תיקונים: יאסרו}. ואף אם הם של רבים מאי הוי. לא יהא אלא אבני הר אפי' ההר של רבים ודאי דמיתסרו. לכן צ"ל דפשיטא תלושין בכל ענין אסורין מה"ט. דזיל בתר טעמא מאי של רבים אינו נאסר. משום שאין אדם אוסר דבר שאינו שלו. וזה דוקא בעודם מחוברים. אבל התולש מים ממעינות של רבים הרי הן שלו דקנינהו בהגבהה. ופשיטא דשלו אוסר. ואפילו בתלושין ועומדין. כיון דאית בהו תפיסת ידי אדם. קמו להו ברשותיה דהך דעבדינהו. ולא של רבים מיקרו:

[^1] ישו' הגון לקושי' הנ"ל מוכרח בטעמו ולמדנו דבתי"א א"ל בין של יחיד לשל רבים


[^2]*והאמת* יורה דרכו שבזה נבין מה שלכאורה אין לו הבנה. דלשיטת המקשים על הש"ך מגמרא הנז' תקשי לדידהו. אמאי דחיק תלמודא לאשכוחי הא דיחיד נאסרין אליבא דר"י בתלשינהו גל או בדטפחינהו בידיה שהם דרכים דחוקים רחוקים וזרים. ומי סני לאוקמי בתלושין {לוח השמטות: בספל}. דהא ר"י סתמא אמר מים של רבים. ולישני דמיירי בתלושין דביחיד כה"ג ודאי אסירי. ואפילו על המקשן היה מקום לדקדק דבאיזה כח מקשה קושיתו ותיפוק ליה דמחוברין נינהו. ואמאי לא אסיק אדעתיה דלמא {לוח תיקונים: דילמא} ר"י מיירי בתלושין כיון דסתמא קאמר. מנ"ל להקשות. אלא ודאי דתלושין גמורין של רבים לא מצית אמרת מטעמא דפרישית. כיון דתלישי וקיימי תו לא מיקרי של רבים. משום דהא אית בהו תפיסת ידי אדם. ופשיטא דאסירי בין של רבים בין של יחיד. משו"ה מיבעי ליה לדחוקי נפשיה דיחיד נאסרין ודכוותיה בשל רבים מותרין. דוקא בדתלשינהו גל או בטפחינהו בידיה. דבשלו נאסרין ודאי דמיקרי תלוש שאוסר השאר בכל שהו. אבל בשל רבים אע"ג דאותם מים דטפחינהו מיקרי תלושין. משו"ה לא אתסרו להו כולהו מיא דבמעיין. לא יהא אלא תלושין שהביאן מביתו שעבדן ושפכן לתוך המעיין דרבים. ודאי לא נאסרו מימיו. דאע"ג דע"ז אינה בטלה ברוב. מ"מ מים של רבים דקביעי במחובר לא מיתסרו. מטעם הנז' שאין אדם אוסר דבר שאינו שלו. תדע דאל"כ יאסר ים המלח וק"ל. וא"כ הרווחנו שיש מן הגמ' הנז' סיוע רב להרב ש"ך ז"ל מדלא בעי לאוקמי לדר"י בתלושין של רבים ודוק היטב.

[^2] וביאור נכון בגמ' דע"ז דנ"ט והרוומנו שאף הנעבד בתלוש אינו אוסר את המחובר של רבים


[^3]*ועוד* אני רואה להפוך בזכותו של הרב ז"ל מצד אחר דאיהו אזיל לטעמיה במ"ש בראש הסי' גבי אבני הר שנתדלדלו שפסק הרמב"ם לקולא. משום דחזקיה שרי ור"י הוא דאסר ע"פ הירושלמי (ואין הלכה כתלמיד במקום הרב אם לא מאביי ורבא ואילך) והשתא דאתינן להכי הך שינוייא דטפחינהו לאו לקושטא דמילתא אסקיה תלמודא. דהא לא איצטרכינן ליה. אלא משום דאי הכי תפשוט דר"י הוא דאמר אסורות. ודחי תלמודא דאי משו"ה לא תפשוט. דאיכא למימר לעולם ר"י שרי והכא מיירי בטפחינהו. אבל אי קיי"ל דר"י אסר. א"כ ל"צ לשינוייא דטפחינהו. אלא שינוייא דתלשינהו הוא עיקר. ודוקא כה"ג כי תלשינהו גל. דלא אסירי אלא משום דדמי לאבני הר שנדלדלו דאסוריה קליש כדלקמן. וחזקיה פליג עליה. הוא דשרי ר"י בדכוותיה בשל רבים. אבל בתלושין ע"י אדם סבירא לן דלא שני לר"י בין של רבים לשל יחיד. דוק ותראה כי טוב הוא. הנה עשינו את שלנו בעז"ה לחפש זכות להרב ז"ל שלא תחול עליו סערת הקושיא הנז' מתלמוד ערוך וצדקה תהיה לנו בין כיון לה הרב בין לא. זכרה לי אלהי לטובה שקיימנו דינו להלכה. אבל לא מטעמיה. כי מ"ש להוכיח כן מהגמרא (דמ"ז) לא נתחוור לי כלל ונ"ל לשון מגומגם. גם ליעיין מר בט"ז. שנגע בגמ' הנ"ל ושלא כדין השיג הפרישה. ואע"פ שיפה פי' דחפירה אינה אוסרת המים. אמנם מה שרצה להוכיח מדלא מצי לאשכוחי כו' לא הבנתי ודוק יעב"ץ ס"ט:

[^3] עוד ד"א יפה אף נעים ליישב דברי הרב ש"ך ונתקיי' בידינו מ"ש שא"ל בתלושים בין של רבים לשל יחיד
