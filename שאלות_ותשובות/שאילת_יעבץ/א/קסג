% שאלה קסג

ר"ח אייר התצג"ל

*צפרא* טבא למר ניהו רבא דעמיה ה"ה כו' כמהור"ר יחזקאל נר"ו:

[^1]*הנה* בעסקנו עכשיו בטא"ח סי' א' שהתחלנו ללמוד עם התלמיד מכ"ז ראינו מ"ש אדוני בספרו שכתב לתרץ קושיות במג"א על הגאונים מהרי"א ודעמיה בענין אשם שבא בנדבה דנזירות. שהרי אשם נזיר אינו אלא כשנטמא וח"ו לצדיקים לטמא את עצמם. ועל זה השיב מעכ"ת דלק"מ דאנחנו כולנו טמאי מתים כו' לפ"ז חייב באשם. ואם אמנם חייב ג"כ בחטאת כו' אך חטאת עוף אינו מכפר כו' א"כ מה יועיל. משא"כ אשם נזיר שוה לשאר אשמות ומכפר על ספק חטאות שפיר יכול לנדר בנזיר עכ"ל:

[^1] בעל כנסת חדשה חזה חזות קשה בישוב קושית במג"א א"ח ס"א בענין אשם נזירות וקריאת הפרשה


*ועמדתי* משתומם על המראה כשעה חדא. הלא משנה שלמה שנינו הנודר בבה"ק אינו מביא קרבן טומאה. דכי כתיב קרבן טומאה בנזיר טהור שנטמא הוא דכתיב. ואנן טמאי מתים אנן. ולא מייתינן ק"ט אף לכשיבנה ב"המ ב"ב. הא חדא:

*וזאת* שנית אפי' לכשתמצי לומר דבר אתויי קרבן הוי. מיהא לא עדיף מנזיר טהור שנטמא. שאין לו קרבן בטומאה. וכמבואר בנזיר פ"ג ופ"ו וברמב"ם דאינו מביא קרבן עד שיזה ויטהר וימנה בטהרה. וזה לא שייך אצלינו. שאין לנו אפר פרה. א"כ מה מועיל אפי' אם באמת נדר בנזיר. אין מציאות לקרבן אשם זה בימינו. שתחשב לו קריאת הפ' כהקרבה. (ומשאר קרבנות לא קשיא כמ"ש בחיבורי בס"ד יעויין משם):

*ועוד* בה שלישיה תמיהא רבתא בדברי מר. דלא דכיר לגמרי הא דכתב הראב"ד שאסור להזיר עכשיו. בכל מקום. ואף הר"מ נ"ל דלא פליג דלכתחלה ודאי אסיר. כדמשמע לשונו פ"ו וק"ל. אע"פ שהכ''מ כתב שלא ידע מה איסור יש בדבר. יעמ''ש בחבורי בס"ד:

[^2]*ועד* ארבעה לו אשיבנו. הא דכתב מר דאשם נזיר שוה לשאר אשמות ומכפר על ספק חטא אחר. פליאה נשגבה בעיני דהא לא דמי לאשם תלוי. דהוא איל בשתי סלעים. ואשם נזיר אינו אלא כבש בן שנתו ככתוב. (כמדומה שבעל החינוך נתן לו מקום לטעות וק"ל) תורה תורה אימרי בדיכרי איחלפו ליה. (ועיין בקידושין דנ"ה) ומשם ג"כ תשובה לדברי האומר שבא בנדבה. וכן מבואר עוד במ"א והארכתי בחיבורי הנ"ל יע"ש) ואיך יעלה לו זה תחת זה. ולא עלה על דעת אדם מעולם לומר כן. זהו מה שנתקשינו בדברי הרב יצ"ו. ומעידני עלי שמים וארץ שזה כמה שנים שעמדתי על דברי הטור ומפרשיו הנ"ל. ויישבתי בעז"ה הרבה דברים שהעירו האחרונים ז"ל בענין זה. נלאיתי להעתיקן הנה. גם קושית במג"א ז"ל נתיישבה אצלי לע"ד. ואחר המו"מ בדבריהם ז"ל כתבתי כלשון הזה. ואכתי אי קשיא לי הא קשיא. אשם הבא ע"י טומאה. היכי משכחת ליה לדידן. דהשתא כולנו טמאי מתים אנן וליכא קרבן טומאה לגמרי. ותו דאסור לידור בטומאה (וכ"כ הראב"ד בפ"ב מהל' נזירות כו' א"כ. פשיטא דאסור לכתחלה ודוק) ועוד שגם הנודר בטומאה אין לו קרבן עד שיזה ויטהר (כדאיתא בנזיר וברמב"ם) והא לא אפשר האידנא. דבעו"ה אין לנו אפר פרה ולא טהרה. והא ודאי לא שייך חיוב קרבן נזיר אצלנו. ולמאי נסיב לה הטור ע"פ שטתם של הגאונים הנ"ל ז"ל. לא ידענא לה פתר. ומי שדעתו רחבה. וימצא פשר דבר שכר הרבה יטול. ואחזיק לו טובה. ע"כ לשוני בספרי. ומי כהחכם יודע פשר כמעלת אדוני. בקשתי יאיר עיני. ויעמידני על האמת אם כוונתי יפה. כי ללמוד אני צריך:

[^2] נשתקע הדבר ולא נאמר כל מאי דאמר איתליה פירכא אגב חרפא שבישתא כיון דעל על ואזיל בתר איפכא
